module gitee.com/k8sdev/admission-registry

go 1.14

require (
	github.com/sirupsen/logrus v1.8.1
	k8s.io/api v0.22.2
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
	k8s.io/klog v1.0.0
)
